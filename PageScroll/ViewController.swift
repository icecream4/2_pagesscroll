//
//  ViewController.swift
//  PageScroll
//
//  Created by Dennis Muck on 04.02.17.
//  Copyright © 2017 Dennis Muck. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewWidthConstraint: NSLayoutConstraint!
    
    var images = [UIImageView]()
    let imageHeight: CGFloat = 150
    let imageWidth: CGFloat = 150

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    /*
        We write all the code in viewDidAppear, because here we have all the frame data (sizes ...) avalaible to us. It is not guaranteed that we have them in the viewDidLoad function (On some views the information is not available in viewDidLoad)
    */
    override func viewDidAppear(_ animated: Bool) {
        var contentWidth: CGFloat = 0.0
        
        /*
            Adapting the width of the scroll view so that it looks good with all the sizes of the iOS devises
        */
        let scrollViewWidth = (view.frame.width * 0.5) + (imageWidth * 0.5) - (imageWidth * 0.2)
        scrollViewWidthConstraint.constant = scrollViewWidth
        view.layoutIfNeeded() //Important!
        
        for index in 0...2 {
            let image = UIImage(named: "icon\(index)") //maybe .png!!!
            let imageView = UIImageView(image: image)
            images.append(imageView)
            
            var newX: CGFloat = 0.0
            newX = scrollViewWidth / 2 + scrollViewWidth * CGFloat(index)
            contentWidth += scrollViewWidth
            
            scrollView.addSubview(imageView)            
            /*
                setting the frame on the image after we added it to scrollView, to make sure that we have the new coordinate system in place (because a view coordinates system is dependant on its parent)
             */
            imageView.frame = CGRect(x: newX - imageWidth / 2, y: (scrollView.frame.height / 2) - (imageHeight / 2), width: imageWidth, height: imageHeight)
        }
        
        /*
            set content size of scroll view so it knows when to stop scrolling or paging
         */
        scrollView.contentSize = CGSize(width: contentWidth, height: view.frame.height)
        
        /*
         We don't want the scrollView to clip what's behind it we wanna show it
         */
        scrollView.clipsToBounds = false
        
        view.addGestureRecognizer(scrollView.panGestureRecognizer)
        
        print("width: \(view.frame.width)")
    }

}

